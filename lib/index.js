'use strict';

/**
 * Module dependencies
 */

/* eslint-disable no-unused-vars */
// Public node modules.
const ImageKit = require("imagekit");

module.exports = {
  init(config) {
    const imagekit = new ImageKit(config.access);

    const upload = (file, customParams = {}) =>
      new Promise((resolve, reject) => {

        const path = file.path ? `${config.params.pathPrefix}/${file.path}/` : `${config.params.pathPrefix}`;
        imagekit.upload({
          file : file.stream || file.buffer,
          fileName : `${file.hash}${file.ext}`,
          folder : path,
          useUniqueFileName: false
        }, (err, result) => {
          if (err) {
            return reject(err);
          }

          file.url = result.url;
          file.provider_metadata = JSON.stringify(result);
          return resolve();
        });
      });

    return {
      uploadStream(file, customParams = {}) {
        return upload(file, customParams);
      },
      upload(file, customParams = {}) {
        return upload(file, customParams);
      },
      delete(file, customParams = {}) {
        return new Promise((resolve, reject) => {
          const metadata = JSON.parse(file.provider_metadata);
          imagekit.deleteFile(metadata.fileId, (err, result) => {
            if (err) {
              return reject(err);
            }

            return resolve();
          });
        });
      },
    };
  },
};
