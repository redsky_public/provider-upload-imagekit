'use strict';

const imagekit = require('imagekit');
const imagekitProvider = require('../index');

jest.mock('imagekit');

const ImageKitInstanceMock = {
  upload: jest.fn((params, callback) => callback(null, {})),
};

imagekit.mockReturnValue(ImageKitInstanceMock);

describe('Imagekit provider', () => {
  const providerInstance = imagekitProvider.init({});

  beforeEach(() => {
    jest.clearAllMocks();
  });

  describe('upload', () => {
    test('Should add url to file object', async () => {
      ImageKitInstanceMock.upload.mockImplementationOnce((params, callback) =>
        callback(null, { url: 'https://validurl.test' })
      );
      const file = {
        hash: 'test',
        ext: 'json',
        mime: 'application/json',
        buffer: '',
      };

      await providerInstance.upload(file);

      expect(ImageKitInstanceMock.upload).toBeCalled();
      expect(file.url).toBeDefined();
      expect(file.url).toEqual('https://validurl.test');
    });
  });
});
